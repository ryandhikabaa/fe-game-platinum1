import React from 'react'
import { shallow } from 'enzyme';
import Index from '../pages/index';

let wrapper;

beforeEach(() => {
  wrapper = shallow(<Index/>)
})

describe('Index', () => {
  it('renders correctly', () => {    
    expect(wrapper).toMatchSnapshot();    
  });

  it('anchor href', () => {        
    let anchorEl = wrapper.find('a').prop('href')

    expect(anchorEl).toBe('/login')    
  })
});