import React from 'react'
import { shallow } from 'enzyme';
import Card from '../components/Card';

describe('Card component', () => {
    it('title card', () => {
        let wrapper = shallow(<Card title="Game Suit"/>);
        let titleEl = wrapper.find('h4').text();

        expect(titleEl).toBe('Game Suit');
    })

    it('link href', () => {
        let wrapper = shallow(<Card/>);
        let linkEl = wrapper.find('Link').prop('href')

        expect(linkEl).toBe('/game/detail')
    })
})