import React, { Component } from 'react'
import { Jumbotron, Container, Table, Button } from 'reactstrap'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faInstagram,
    faFacebook,
    faTwitter,
    faTwitch,
} from "@fortawesome/free-brands-svg-icons";
import styled from 'styled-components';
import Navbar from '../components/Navbar'
import Card from '../components/Card'
import auth from '../auth/auth';


const Styled = styled.div`

    .jumbo{
        background-color: rgb(255, 255, 255);
        height: 500px;
        position: relative;
        padding: 2rem;
    }
    .Titlec {
        color: #000000;
        font-family: Arial;
        font-style: normal;
        font-weight: bold;
        padding-top: 5rem;
        font-size: 75px;
        text-align: left;
        letter-spacing: 3px;
        text-transform: uppercase;
        
    }

    .card {
        width: 370px;
        height: 400px;
        margin-top: 30px;    
        margin-left: 20px;
    }

    .btn{
        width: 15rem;
        height: 5rem;
        position: absolute;
    }
    .game2{
        position: relative;
        float: right;
        top: -23rem;
        right: -21rem;
        width: 400px;
        transform: rotateZ(-30deg);
    }
    .game1{
        position: relative;
        float: right;
        height: 350px;
        top: -15rem;
        right: -7rem;
    }
    .text {
        font-size: 24px;
        color: #000000;
        font-family: Arial;
        font-style: normal;
        font-weight: normal;

        letter-spacing: 2px;
    }

    .image {
        width: 90%;
        height: 210px;
        margin-top: 1rem;
        margin-left: 1rem;
        margin-right: 1rem;
    }
    .button{
        width: 150px;
        height: 70px;
        margin-left: 1.2rem;
        margin-bottom: 1rem;
    }
    .con {
        margin-top: 50px;     
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }
    .title{
        margin-left: 1rem;
        margin-bottom: 2rem;
        margin-top: 2rem;
    }
    .form {
        width: 40%;
        float: right;
        margin-right: 10%;
    }
    .imgc {
        margin-top: 1rem;
        
        float: right;
    }
    .content1 {
        display: flex;
        justify-content: space-between;
    }
    .table {
        margin-top: 20rem;
        height: auto;
        transform: translateY(-10rem);
        margin-left: 9rem;
        margin-right: 10rem;
    }
    .judul{
        text-align: center;
        transform: translateY(6rem);
    }
    .tes {
        text-align: center;
        margin-top: 5rem;
        text-transform: uppercase;
    }
    .tes1 {
        text-align: center;
        margin-top: 2rem;
        text-transform: capitalize;
    }
    .footimage {
        display: flex;
        justify-content: space-around;
        align-items: center;
    }
    .img1 {
        width: 375px;
        height: 545px;
        margin-left: 7rem;
    }
    .img2{
        width: 278px;
        height: 323px;
    }
    .img3{
        margin-right: 9rem;
    }
    .sosmed{
        display: flex;
        justify-content: space-around;
    }
    .sosmed a{
        text-decoration: none;
        color: black;
    }
    
    
`

class home extends Component {
    render() {
        return (
            <>
                <Navbar />
                <Styled>
                    <div>
                        <div>
                            <Jumbotron fluid className="jumbo">
                                <Container >
                                    <h1 className="Titlec">GAME STORE</h1>
                                    <br /><br />
                                    <p className="text">There are many games here!</p>
                                    <br />
                                    <Button color="primary" size="lg" className="btn">Find Game</Button><br />
                                    <img src="/game1.png" alt="" className="game1" />
                                    <img src="/game2.png" alt="" className="game2" />
                                </Container>
                            </Jumbotron>
                        </div>

                        {/* Tempat card */}
                        <Card />


                        {/* Content */}
                        <div>
                            <h1 className="judul"> Spek Requirement</h1>
                            <div className="content1">
                                <Table hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>SYSTEM </th>
                                            <th>Minimum</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Windows 7 64-bit only</td>
                                            <td>Intel Core 2 Duo @ 2.4 GHZ</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>AMD</td>
                                            <td>AMD Athlon X2 @ 2.8 GHZ</td>
                                        </tr>
                                    </tbody>
                                </Table>
                                <img src="/img.png" alt="" className="imgc" />
                            </div>
                        </div>

                        {/* Footer */}
                        <div className="footerq">
                            <h1 className="tes"> Ayo buruan mainkan game favorit kamu sekarang</h1>
                            <h3 className="tes1"> Disini Juga Terdapat Husbu Dan Waifu Kamu</h3>

                            <br />
                            <br />
                            <div className="footimage">
                                <img src="/imgab.png" alt="" className="img1" />
                                <img src="/imgcd.png" alt="" className="img2" />
                                <img src="/imgef.png" alt="" className="img3" />
                            </div>
                            <div className="sosmed">
                                <h3>
                                    <FontAwesomeIcon icon={faInstagram} size="lg" />{" "}
                                    <a href="#">@BinarRusK2</a>
                                </h3>
                                <h3>
                                    <FontAwesomeIcon icon={faFacebook} size="lg" />{" "}
                                    <a href="#">BinarRusK2</a>
                                </h3>
                                <h3>
                                    <FontAwesomeIcon icon={faTwitter} size="lg" />{" "}
                                    <a href="#">BinarRusK2</a>
                                </h3>
                                <h3>
                                    <FontAwesomeIcon icon={faTwitch} size="lg" />{" "}
                                    <a href="#">BinarRusK2</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </Styled>
            </>
        )
    }
}

export default auth(home)