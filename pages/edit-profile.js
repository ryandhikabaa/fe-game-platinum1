import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios'
import router, { withRouter } from 'next/router'
import Navbar from '../components/Navbar'

const Style = styled.div`

    /* background-color: #ffffff;    
    width: 447px;
    height: 655px;
    margin-top: 4%;
    margin-left: 35%; */

    position: fixed;
    top: 9%;
    left: 35%;    
    background-color: #ffffff;
    width: 447px;
    height: 655px;

    h1 {
        text-align: center;
    }

    .form {
        margin-top: 25px !important;        
    }    

    Button {
        margin-top: 50px;
        width: 100%;
        height: 50px;
        border-radius: 0;
    }

    
    .container {
        margin-top: 25%;
        width: 90%;
    }
    
`


class LoginPage extends Component {

    state = {
        email: '',
        username: ''
    }

    handleEmailChange = event => {
        this.setState({ email: event.target.value });
    }

    handleUsernameChange = event => {
        this.setState({ name: event.target.value });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const user = {
            username: this.state.username,
            email: this.state.email
        };


        axios.post(`http://localhost:3333/api/v1/login`, user)
            .then(res => {
                console.log(res);
                console.log(res.data);

            })
            .catch(error => {
                console.log(error.response);
            })
    }

    render() {
        return (
            <>
                <Navbar />
                <Style>
                    <div className="container">
                        <h1>EDIT PROFILE</h1>
                        <Form method="post" onSubmit={this.handleSubmit}>
                            <FormGroup className="form">
                                <Label for="exampleUsername">Username</Label>
                                <Input type="text" name="Username" id="exampleUsername" value={this.state.username} onChange={(event) => { this.handleUsernameChange(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="exampleEmail">Email</Label>
                                <Input type="email" name="email" id="exampleEmail" value={this.state.email} onChange={(event) => { this.handleEmailChange(event) }} />
                            </FormGroup>
                        </Form>
                        <Button color="warning" onClick={(e) => {
                            this.handleSubmit(e)
                            router.push('/profile')
                        }}>EDIT PROFILE</Button>
                    </div>
                </Style>
            </>
        )
    }
}
export default withRouter(LoginPage);