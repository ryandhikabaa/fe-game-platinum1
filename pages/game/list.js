import React, { Component } from 'react'
import styled from 'styled-components'
import Navbar from '../../components/Navbar'
import Card from '../../components/Card'
import auth from '../../auth/auth'

const Styled = styled.div`        

    .card {
        width: 300px;
        height: 320px;
        margin-top: 30px;    
        margin-left: 20px;
    }

    .image {
        width: 100%;
        height: 200px;
    }

    .body {
        margin-top: 15px;
        margin-left: 15px;
    }

    .container {
        margin-top: 50px;     
        display: flex;
        flex-wrap: wrap;
    }

    Button {
        margin-top: 10px;
    }
        
`


class GameList extends Component {

    state = {
        title: ''
    }

    render() {
        return (
            <>
                <Navbar/>
                <Card title="Game Suit"/>
            </>
        )
    }
}

export default auth(GameList)