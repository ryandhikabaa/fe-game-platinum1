import React, { Component } from "react";
import { Table } from "reactstrap";
import styled from "styled-components";
import GameSuit from "../../components/GameSuit";
import Navbar from "../../components/Navbar";
import ModalExample from "../../components/Modal";
import auth from "../../auth/auth";
import axios from "axios";

const Styled = styled.div`
  .container {
    width: 1290px;
    height: 600px;
    margin-top: 20px;
  }

  .leaderboard {
    margin-top: 50px;
  }
`;

class GameDetailPage extends Component {
  state = {
    rating: 0,
    leaderBoard: [],
    modal: false,
  };

  getData = () => {
    axios
      .get("https://quiet-crag-17476.herokuapp.com/api/v1/showallgame")
      .then((res) => {
        console.log(res.data);
        this.setState({
          leaderBoard: res.data,
        });
        console.log("ini leaderboard", this.state.leaderBoard);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  componentDidMount() {
    this.setState({
      modal: false,
    });

    this.getData();
  }

//   rating = () => {
//       this.setState({
//           rating: this.state.rating + 1
//       })

//       console.log(this.state.rating);
//       return `<td>{this.state.rating}</td>`
//   }

  render() {
    return (
      <>
        <Navbar />
        <Styled>
          <div className="modal">
            {this.state.modal ? <ModalExample /> : null}
          </div>
          <div className="container">
            <GameSuit />
          </div>
          <div className="container leaderboard">
            <Table striped>
              <thead>
                <tr>                  
                  <th>Username</th>
                  <th>Scores</th>
                </tr>
              </thead>
              <tbody>
                {this.state.leaderBoard.map((data) => (
                  <tr>                    
                    <td>{data.user.username}</td>
                    <td>{data.score}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Styled>
      </>
    );
  }
}

export default auth(GameDetailPage);
