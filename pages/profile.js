import React, { Component } from 'react'
import styled from 'styled-components';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap'
import Navbar from '../components/Navbar'
import axios from 'axios';
import auth from '../auth/auth';

const Styled = styled.div`
    position: fixed;
    top: 9%;
    left: 35%;    
    background-color: #ffffff;
    width: 447px;
    height: auto;

    img {
        border-radius: 50%; 
        width: 160px;
        height: 160px;
        display: block;
        margin-left: auto;
        margin-right: auto;
        margin-top: 5%;
        margin-bottom: 50px;
    }

    h2 {
        text-align: center;
        margin-top: -1%;
    }    

    p {
        font-size: 18px;
        margin-top: -10px;
    }

    h6 {
        color: grey;
        font-size: 14px;
    }
    
    Button {
         margin-top: 19px ;
        width: 100%;
        height: 50px;
        border-radius: 0;
    }
    
    .container {
        width: 90%;
        margin-top: 50px;
        margin-bottom: 50px;
    }
`

class ProfilePage extends Component {

    state = {
        username: '',
        email: '',
        userId: '',
        name: '',
        address: '',
        notelp: '',
        image: 'https://cdn0-production-images-kly.akamaized.net/V-K5ihJ2k0XQ2xSnhvP7mQGKOXE=/1231x710/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2938534/original/086505200_1571035347-AP1.jpg'


    }
    handlename = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    handleAddress = (event) => {
        this.setState({
            address: event.target.value
        })
    }
    handlenoTelp = (event) => {
        this.setState({
            notelp: event.target.value
        })
    }
    // show(biodata) {
    //     if (biodata) {
    //         this.setState({
    //             name: biodata.name,
    //             address: biodata.address,
    //             notelp: biodata.notelp
    //         })
    //     }
    // }
    async componentDidMount() {
        const data = JSON.parse(localStorage.getItem("user"));
        console.log(data.id);

        if(data){
            await axios.get(`https://quiet-crag-17476.herokuapp.com/api/v1/biodata/${data.id}`).
            then(res => {
                console.log(res.data);                
                localStorage.setItem("biodata", JSON.stringify(res.data))
            })
            .catch(error => {
                console.log(error.response);
            })
        }

        
        const biodata = JSON.parse(localStorage.getItem("biodata"));
        this.setState({
            userId: data.id,
            username: data.username,
            email: data.email,

        })
        if (biodata) {
            this.setState({
                name: biodata.name,
                address: biodata.address,
                notelp: biodata.notelp
            })
        }

    }
    handleDataLocal = e => {
        localStorage.setItem("biodata", JSON.stringify(e));
        console.log(localStorage.getItem("biodata"));
    }
    handleSubmit = () => {
        const biodata = {
            userId: this.state.userId,
            name: this.state.name,
            address: this.state.address,
            notelp: this.state.notelp
        }
        axios.post(`https://quiet-crag-17476.herokuapp.com/api/v1/biodata`, biodata).
            then(res => {
                console.log(res.data);
                this.handleDataLocal(res.data);
            })
            .catch(error => {
                console.log(error.response);

            })
    }
    handleUpdate = () => {
        const data = JSON.parse(localStorage.getItem("user"));
        const biodata = {
            userId: this.state.userId,
            name: this.state.name,
            address: this.state.address,
            notelp: this.state.notelp
        }
        axios.put(`https://quiet-crag-17476.herokuapp.com/api/v1/biodata/${data.id}`, biodata).
            then(res => {
                console.log(res.data);
                alert(" Data Berhasil Di Update! ");
            })
            .catch(error => {
                console.log(error.response);

            })
    }
    render() {
        return (
            <>
                <Navbar />
                <Styled>
                    <div className="container">
                        <h2>My Profile</h2>
                        <img src={this.state.image} alt={this.props.alt} />
                        <div className="profile">
                            <h6>Username</h6>
                            <p>{this.state.username}</p>
                        </div>
                        <div className="profile">
                            <h6>Email</h6>
                            <p>{this.state.email}</p>
                        </div>
                        <Form method="post" onSubmit={this.handleSubmit}>

                            <FormGroup className="form">
                                <Label for="exampleEmail">Nama</Label>
                                <Input type="text" name="name" id="exampleEmail" value={this.state.name} onChange={(event) => { this.handlename(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="address">Alamat</Label>
                                <Input type="text" name="address" id="address" value={this.state.address} onChange={(event) => { this.handleAddress(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="notelp">No Telp</Label>
                                <Input type="text" name="notelp" id="notelp" value={this.state.notelp} onChange={(event) => { this.handlenoTelp(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Input type="hidden" name="userId" id="userId" value={this.state.userId} disabled />
                            </FormGroup>
                        </Form>
                        <br />
                        <Button color="warning" onClick={this.handleSubmit}>Edit Profile</Button>{' '}<br />
                        <Button color="warning" onClick={this.handleUpdate}>Update Profile</Button>{''}


                    </div>
                </Styled>
            </>
        )
    }
}

export default auth(ProfilePage)