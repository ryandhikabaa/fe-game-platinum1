import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios'
import router, { withRouter } from 'next/router'

const Style = styled.div`

    /* background-color: #ffffff;    
    width: 447px;
    height: 655px;
    margin-top: 4%;
    margin-left: 35%; */
    .register{
    position: relative;
    /* top: 9%;
    left: 35%;     */
    margin: 5% auto;
    background-color: #ffffff;
    width: 447px;
    height: 655px;
    padding: 1%;
    }
    h1 {
        text-align: center;
    }

    .form {
        margin-top: 25px !important;        
    }    
    a {
        float:right;
        margin-top:1rem;
    }
    Button {
        margin-top: 50px;
        width: 100%;
        height: 50px;
        border-radius: 0;
    }

    
    .container {
        margin-top: 25%;
        width: 90%;
    }
    @media(max-width : 800px){
    body {
        overflow-y: hidden;
    }
    .register{
        width: 100%;
        height: auto;
        margin: auto;
        padding: 15%;
    }
    .container{
        margin-top: 15%;
        margin-bottom: 30%;
       
    }
}
@media(max-width : 700px){
    body {
        overflow-y: hidden;
    }
    .register{
        width: 100%;
        height: auto;
        margin: auto;
        padding: 10%;
    }
    .container{
        margin-top: 28%;
        margin-bottom: 30%;
       
    }
    h1 {
        font-size: 4rem;
    }
}
@media (max-width:600px){
    .container{
        margin-top: 28%;
        
       
    }
    body {
        background-color: #ffffff !important ;
    }
}
`


class Register extends Component {

    state = {
        username: '',
        email: '',
        password: ''
    }

    handleUsernameChange = event => {
        this.setState({ username: event.target.value });
    }

    handleEmailChange = event => {
        this.setState({ email: event.target.value });
    }

    handlePasswordChange = event => {
        this.setState({ password: event.target.value });
    }
    handleDataLocal = event => {
        localStorage.setItem("user", JSON.stringify(event));
        console.log(localStorage.getItem("user"))
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const user = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password
        };


        axios.post(`https://quiet-crag-17476.herokuapp.com/api/v1/register`, user)
            .then(res => {
                console.log(res);
                console.log(res.data);
                this.handleDataLocal(res.data);
                router.push('/home')
            })
            .catch(error => {
                console.log(error.response);
                alert(" Email already exists! ");
                router.push('/register')
            })
    }

    render() {
        return (
            <Style>
                <div className="register">
                    <div className="container">
                        <h1>SIGN UP</h1>
                        <Form method="post" onSubmit={this.handleSubmit}>
                            <FormGroup className="form">
                                <Label for="exampleName">Username</Label>
                                <Input type="text" name="username" id="exampleName" value={this.state.username} onChange={(event) => { this.handleUsernameChange(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="exampleEmail">Email</Label>
                                <Input type="email" name="email" id="exampleEmail" value={this.state.email} onChange={(event) => { this.handleEmailChange(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="examplePassword">Password</Label>
                                <Input type="password" name="password" id="examplePassword" value={this.state.password} onChange={(event) => { this.handlePasswordChange(event) }} />
                            </FormGroup>
                        </Form>
                        <a href="/login">You Have Account? Login Now</a>
                        <Button color="warning" type="submit" onClick={(e) => {
                            this.handleSubmit(e)

                        }}>SIGN UP</Button>
                    </div>
                </div>
            </Style>
        )
    }
}

export default withRouter(Register)