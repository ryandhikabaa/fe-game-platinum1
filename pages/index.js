import React from 'react'

function LandingPage(){
    return(
        <div id='main-landingpage'>
            {/* <Navbar/> */}
            <div className='name'>
                <h1><span>Launch Your Game</span> With Confidence And Creativity</h1>
                <p className='details'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <a href='/login' className='cv-btn'>Join Now!</a>
            </div>
        </div>
    )
}

export default LandingPage;