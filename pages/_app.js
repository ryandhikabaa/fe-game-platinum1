import '../styles/index.css'
// import '../styles/login.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux';
import initialState from '../redux/reduxReducer';
import thunk from 'redux-thunk';
const redux = require('redux')
const storea = redux.createStore(initialState, redux.applyMiddleware(thunk));

function MyApp({ Component, pageProps }) {
  return <Provider store={storea}><Component {...pageProps} /></Provider>
}

export default MyApp
