import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios'
import router, { withRouter } from 'next/router'

const Style = styled.div`

    /* background-color: #ffffff;    
    width: 447px;
    height: 655px;
    margin-top: 4%;
    margin-left: 35%; */
    .login {

position: relative;
/* top: 9%;
left: 35%;     */
margin: 5% auto;
/* margin-top: 5%; */
background-color: #ffffff;
width: 447px;
height: 655px;
padding: 1%;
}

h1 {
    text-align: center;
}
a {
    float:right;
    margin-top:1rem;
    margin-bottom: 1rem;
}
.form {
    margin-top: 25px;        
}    

Button {
    margin-top: 50px;
    width: 100%;
    height: 50px;
    border-radius: 0;
}


.container {
    margin-top: 25%;
    width: 90%;
}
@media(max-width : 800px){
    body {
        overflow-y: hidden;
    }
    .login{
        width: 100%;
        height: auto;
        margin: auto;
        padding: 15%;
    }
    .container{
        margin-top: 15%;
        margin-bottom: 30%;
       
    }
}
@media(max-width : 700px){
    body {
        overflow-y: hidden;
    }
    .login{
        width: 100%;
        height: auto;
        margin: auto;
        padding: 10%;
    }
    .container{
        margin-top: 28%;
        margin-bottom: 30%;
       
    }
    h1 {
        font-size: 4rem;
    }
}
@media (max-width:600px){
    .container{
        margin-top: 28%;
        
       
    }
    body {
        background-color: #ffffff !important ;
    }
}
`


class LoginPage extends Component {

    state = {
        email: '',
        password: '',
        name1: 'Email'
    }

    handleEmailChange = event => {
        this.setState({ email: event.target.value });
    }

    handlePasswordChange = event => {
        this.setState({ password: event.target.value });
    }
    handleDataLocal = e => {
        localStorage.setItem("user", JSON.stringify(e));
        console.log(localStorage.getItem("user"));
    }
    handleSubmit = async (event) => {
        event.preventDefault();

        const user = {
            email: this.state.email,
            password: this.state.password
        };


        await axios.post(`https://quiet-crag-17476.herokuapp.com/api/v1/login`, user)
            .then(res => {
                console.log(res.data);
                this.handleDataLocal(res.data);
                router.push('/home')
            })
            .catch(error => {
                console.log(error.response);
                alert(" Username Atau Password Salah!")
                router.push('/login')
            })
                    
    }

    render() {
        return (
            <Style>
                <div className="login">
                    <div className="container">
                        <h1>SIGN IN</h1>
                        <Form method="post" onSubmit={this.handleSubmit}>
                            <FormGroup className="form">
                                <Label for="exampleEmail">Email</Label>
                                <Input type="email" name="email" id="exampleEmail" value={this.state.email} onChange={(event) => { this.handleEmailChange(event) }} />
                            </FormGroup>
                            <FormGroup className="form">
                                <Label for="examplePassword">Password</Label>
                                <Input type="password" name="password" id="examplePassword" value={this.state.password} onChange={(event) => { this.handlePasswordChange(event) }} />
                            </FormGroup>
                        </Form>
                        <a href="/register">You Don't Have Account?</a>
                        <Button color="warning" onClick={(e) => {
                            this.handleSubmit(e)
                            router.push('/home')
                        }}>SIGN IN</Button>
                    </div>
                </div>
            </Style>
        )
    }
}

export default withRouter(LoginPage)
