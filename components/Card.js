import React, { Component } from 'react'
import { Button } from 'reactstrap'
import styled from 'styled-components'
import Link from 'next/link'

const Styled = styled.div`
    .card {
        width: 300px;
        height: 320px;
        margin-top: 30px;    
        margin-left: 20px;
    }

    .image {
        width: 100%;
        height: 200px;
    }

    .body {
        margin-top: 15px;
        margin-left: 15px;
    }

    .container {
        margin-top: 50px;     
        display: flex;
        flex-wrap: wrap;
    }

    Button {
        margin-top: 10px;
    }
`

export default class Card extends Component {
    render() {
        return (
            <div>
                <Styled>
                    <div className="container">
                        <div className="card">                        
                            <img className="image" alt="game-img" src="https://static.vecteezy.com/system/resources/previews/000/691/497/original/rock-paper-scissors-neon-icons-vector.jpg"/>                        
                            <div className="body">
                                <h4>{this.props.title}</h4>
                                <Link href="/game/detail">
                                    <Button color="primary">Play Now</Button>  
                                </Link>                                                      
                            </div>
                        </div>                                    
                    </div>        
                </Styled>
            </div>
        )
    }
}
