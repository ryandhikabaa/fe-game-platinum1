import { Button, Badge} from 'reactstrap'
import React, { Component } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const Styled = styled.div`

    .container {
        background: rgb(2,0,36);
        background: linear-gradient(140deg, rgba(2,0,36,1) 0%, rgba(1,108,148,1) 69%, rgba(0,212,255,1) 100%);
        width: 1290px;
        height: 600px;
        display: flex;
    }

    .choice {           
        margin-left: 50px;
        padding-top: 30px;
    }

    .choice img{
        width: 100px;
    }

    .choice img:hover{
        opacity: 0.5;
    }

    .choice Button {
        background-color: transparent;
        border: none;        
    }

    .choice Button:hover {
        background-color: transparent;
        border: none;
    }

    .down {
        margin-top: 250px;
    }

    .score {
        margin-top: 50px;
        margin-left: 140px;
    }

    .com img {
        width: 250px;
    }

    .com {
        margin-top: 150px;
        margin-left: 150px;
    }

    .com Button {
        background-color: transparent;
        border: none;  
    }

    .com Button:hover {
        background-color: transparent;
        border: none;
    }
`

export default class GameSuit extends Component {    
    state = {
        id: null,
        scores: 0,
        playerChoice: null,
        comChoice: null,
        image: '/rock.png',
        isPlayerWin: false
    }
    
    comRandomPick = async () => {
        const choices = ["", "rock", "paper", "scissors"];
        await this.setState({
            // comChoice: choices[Math.floor(Math.random() * 3)], 
            comChoice: Math.floor(Math.random() * 3) + 1
        });        

        this.setState({image: `/${choices[this.state.comChoice]}.png`})
      };

    handleChangeScissors = () => {        
        this.setState({
            playerChoice: 1,            
        })       
        console.log(this.state.playerChoice);   
    }

    handleChangeRock = () => {
        this.setState({playerChoice: 2})    
        console.log(this.state.playerChoice);      
    }

    handleChangePaper = () => {
        this.setState({playerChoice: 3}) 
        console.log(this.state.playerChoice);         
    }
    
    playerWin = () => {                          
        setTimeout(() => {
            if(this.state.isPlayerWin == true) return alert("Player win!!!")
            else if(this.state.isPlayerWin == false) return alert("Player lose!!")
        }, 800)
    }

    // Mengirimkan data berupa pilihan user, computer, dan user id
    postData = () => {         

        // console.log("INI postData", JSON.parse(localStorage.getItem("user")).id);

        // let id = JSON.parse(localStorage.getItem("user")).id
        // console.log(id);

        const choice = {
            userId: JSON.parse(localStorage.getItem("user")).id,
            datauser: this.state.playerChoice,
            datacomp: this.state.comChoice
        }

        console.log(choice.userId);

        axios.post('https://quiet-crag-17476.herokuapp.com/api/v1/game', choice)
            .then(res => {
                console.log(res.data);
                this.setState({
                    scores: res.data.score
                })
            }).catch(err => {
                
            })
    }

    
      

    render() {
        return (
            <Styled>
                <div className="container">
                    <div className="choice">                        
                        <Button className="down"
                            onClick={async (e) => {                                
                                await this.handleChangeScissors()  
                                this.comRandomPick()  
                                this.postData()                                                       
                            }}
                        ><img src="/scissors.png" alt="scissors"/></Button>

                        <Button
                            onClick={async () => {
                                await this.handleChangeRock()
                                this.comRandomPick()         
                                this.postData()                       
                            }}
                        ><img src="/rock.png" alt="rock"/></Button>

                        <Button className="down"
                            onClick={async () => {
                                await this.handleChangePaper()
                                this.comRandomPick()          
                                this.postData()                      
                            }}
                        ><img src="/paper.png" alt="paper"/></Button>
                    </div>
                    <div className="score">
                        <Button color="primary">
                            Scores <Badge color="secondary">{this.state.scores}</Badge>
                        </Button>
                    </div>
                    <div className="com">                        
                        <img src={this.state.image} alt="rock"/>
                    </div>                                
                </div>
            </Styled>
        )
    }
}














// result = async () => {
    //     let player = this.state.playerChoice
    //     let com = this.state.comChoice
    //     let scores = this.state.scores

    //     //Win
    //     if(player == "paper" && com == "rock") {
    //         await this.setState({isPlayerWin: true})
    //         this.setState({scores: scores + 1})
    //         this.playerWin()
    //     }
    //     else if(player == "rock" && com == "scissors") {
    //         await this.setState({isPlayerWin: true})
    //         this.setState({scores: scores + 1})
    //         this.playerWin()
    //     }
    //     else if(player == "scissors" && com == "paper") {
    //         await this.setState({isPlayerWin: true})
    //         this.setState({scores: scores + 1})
    //         this.playerWin()
    //     }

    //     // Lose
    //     else if(player == "rock" && com == "paper") {    
    //         await this.setState({isPlayerWin: false})          
    //         this.setState({scores: scores - 1}) 
    //         this.playerWin()         
    //     }
    //     else if(player == "paper" && com == "scissors") {     
    //         await this.setState({isPlayerWin: false})            
    //         this.setState({scores: scores - 1})   
    //         this.playerWin()       
    //     }
    //     else if(player == "scissors" && com == "rock") {
    //         await this.setState({scores: scores - 1})          
    //         this.setState({isPlayerWin: false})                       
    //         this.playerWin()         
    //     }

    //     // Draw
    //     else if(player == "scissors" && com == "scissors" || player == "rock" && com == "rock" || player == "paper" && com == "paper"){
    //         await alert("Draw")
    //     }
    //     console.log(this.state.isPlayerWin);
    // }      