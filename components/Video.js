import React, {Component} from 'react';
import styled from 'styled-components';
import ReactPlayer from 'react-player'

const Styled = styled.div`

    .wrapper {
        width: 100%;    
        height: 25rem;
        margin: 0 auto;        
    }

`

export default class Video extends Component {
    render() {
        return (
            <Styled>
                <div className="wrapper">
                    <ReactPlayer
                        controls
                        className='react-player'
                        url='https://www.youtube.com/watch?v=2dsHuU10udY&t=10s'
                        width='100%'
                        height='100%'
                    />
                </div>
            </Styled>
        )
    }
}
