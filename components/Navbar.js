import React, { useState, useEffect } from 'react';
import router, { withRouter } from 'next/router'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import styled from 'styled-components';


const Styled = styled.div`

`


const NavbarComponent = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [label, setLabel] = useState('LOGIN');
  const [label1, setLabel1] = useState('REGISTER');
  const [param, setParam] = useState('login');
  const [param1, setParam1] = useState('register');


  // akhir data
  // ambil & set text
  function getData(get) {
    if (get) {
      setLabel(get.username);
      setLabel1('LOGOUT');
      if (label == get.username) {
        setParam('profile');
      }
      if (label1 == 'LOGOUT') {
        setParam1('/');
      }
    }

  }


  useEffect(() => {
    // data
    getData(JSON.parse(localStorage.getItem("user")));
  })

  function click() {
    if (label) {
      localStorage.removeItem("user");
      localStorage.removeItem("biodata");
    }
  }
  // akhir




  const toggle = () => setIsOpen(!isOpen);

  return (
    <Styled>

      <Navbar color="light" light expand="md">
        <div className="container">
          <NavbarBrand href="/" className="me-5">
            <img
              alt="brand-img"
              src="/logoapp.png"
              width="170"
              height="30"
              className="d-inline-block align-top"
            />
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink href="/home">HOME</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/game/list">GAME LIST</NavLink>
              </NavItem>                   
            </Nav>
            <Nav className="ms-auto" navbar>
              <NavItem>
                <NavLink href={param}>{label}</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href={param1} onClick={() => { click() }}>{label1}</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </div>
      </Navbar>
    </Styled>
  );
}

export default NavbarComponent;