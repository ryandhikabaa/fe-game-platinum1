import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import styled from 'styled-components';
import Video from './Video';

const Styled = styled.div`


`

const ModalExample = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(true);

  const toggle = () => setModal(!modal);

  return (
    <Styled>      
      <div className="modal">
        <Modal size="lg" isOpen={modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
            toggle={toggle} className={className}>
            <ModalHeader toggle={toggle}>How to Play</ModalHeader>
            <ModalBody>                
                <Video/>
            </ModalBody>    
        </Modal>
      </div>
    </Styled>
  );
}

export default ModalExample;