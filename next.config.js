module.exports = {
    reactStrictMode: true,
    images: {
      domains: ['https://www.ga.berkeley.edu'],
    },
    eslint: {
      // Warning: Dangerously allow production builds to successfully complete even if
      // your project has ESLint errors.
      ignoreDuringBuilds: true,
    },
  }
  